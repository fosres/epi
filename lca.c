#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

struct node {
    
    	struct node *left;
    
	struct node *right;
    
	int data;
  
};

struct node* insert( struct node* root, int data ) {
		
	if (root == NULL) {
	
        struct node* node = (struct node*)malloc(sizeof(struct node));

        node->data = data;

        node->left = NULL;
        node->right = NULL;
        return node;
	  
	} else {
      
		struct node* cur;
		
		if(data <= root->data) {
            cur = insert(root->left, data);
            root->left = cur;
		} else {
            cur = insert(root->right, data);
            root->right = cur;
		}
	
		return root;
	}
}

/* you only have to complete the function given below.  
node is defined as  

struct node {
    
    int data;
    struct node *left;
    struct node *right;
  
};

*/

typedef struct node node;

unsigned lc = 0, rc = 0;

void preorder(node * r, int v1, int v2, unsigned * c)
{
	if 	( r == 0 )
	{
		return;
	}
	
	if	(
			(r->data == v1)

			||

			(r->data == v2)
		)
	{
		(*c)++;
	}

	preorder(r->left,v1,v2,c);

	preorder(r->right,v1,v2,c);
}

struct node *lca( struct node *root, int v1, int v2 ) 
{
	lc = 0;

	rc = 0;

	preorder(root->left,v1,v2,&lc);

	preorder(root->right,v1,v2,&rc);

	printf("lc: %d rc: %d\n",lc,rc);

	if ( lc == 2 )
	{
		return lca(root->left,v1,v2);
	}

	else if ( rc == 2 )
	{
		return lca(root->right,v1,v2);	
	}

	else if (
			(lc == 1)

			&&

			(rc == 1)

		)
	{
		printf("%p\n%d\n",root,root->data);

		return root;
	}

	else return root;
}


int main() {
  
    struct node* root = NULL;
    
    int t;
    int data;

    scanf("%d", &t);

    while(t-- > 0) {
        scanf("%d", &data);
        root = insert(root, data);
    }
  	int v1;
  	int v2;
  
  	scanf("%d%d", &v1, &v2);
	struct node *ans = lca(root, v1, v2);
  	printf("%d", ans->data);
  	
    return 0;
}

