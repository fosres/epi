#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(void)
{
	unsigned char afreq['z'-'a'+1], bfreq['z'-'a'+1];

	memset(afreq,0x0,('z'-'a'+1)*sizeof(unsigned char));

	memset(bfreq,0x0,('z'-'a'+1)*sizeof(unsigned char));

	unsigned char res[5001], * resp = &res[0]-1;

	memset(res,0x0,5001*sizeof(unsigned char));

	unsigned char c = 0;
	
	unsigned i = 0, len = 0;

	while 	(

			( (c = fgetc(stdin)) )

			&&
			
			( c >= 'a' )

			&&

			( c <= 'z' )
			
			&&

			( (++i) <= 100000 )		

		)

	{
		afreq[c-'a']++;
	}

	if ( c != 0x20 )
	{
		fprintf(stderr,"Error: Improperly formatted standard input.\nFound hex code:0x%.2x\n",c);

		exit(255);
	}

	i = 0;

	while 	(

			( (c = fgetc(stdin)) )

			&&
			
			( c >= 'a' )

			&&

			( c <= 'z' )
			
			&&

			( (++i) <= 5000 )		

		)

	{
		if ( bfreq[c-'a'] < afreq[c-'a'] )
		{
			*++resp = c;

			len++;
		}

		bfreq[c-'a']++;
	}

/*
	if ( res[0] == 0x0 )
	{
		printf("-");
	}

	else
	{
		printf("%s",res);
	}
*/
	printf("%u",len);

	return 0;
}
