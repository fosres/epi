#include <stdio.h>
#include <stdlib.h>

void update(unsigned *a,unsigned *b) {
    // Complete this function   

    // Last integer vulnerabilities are here
    int a_old = *a;

    int b_old = *b;

    if 	(
		( a_old < 0 )
		
		&&
		
		( b_old < 0 )
		
		&&

		( (a_old + b_old) > 0 )

	)
	{
		fprintf(stderr,"Error Integer Overflow vulnerability detected\n");

		exit(1);	
	}	
    else if 	(
			( a_old > 0 )
			
			&&
			
			( b_old > 0 )
			
			&&

			( (a_old + b_old) < 0 )

	)
	{
		fprintf(stderr,"Error Integer Overflow vulnerability detected\n");

		exit(1);	
	}	
    
    *a = a_old + b_old;

    b_old = -b_old;

    if 	(
		( a_old < 0 )
		
		&&
		
		( b_old < 0 )
		
		&&

		( (a_old + b_old) > 0 )

	)
	{
		fprintf(stderr,"Error Integer Overflow vulnerability detected\n");

		exit(1);	
	}	
    else if 	(
			( a_old > 0 )
			
			&&
			
			( b_old > 0 )
			
			&&

			( (a_old + b_old) < 0 )

	)
	{
		fprintf(stderr,"Error Integer Overflow vulnerability detected\n");

		exit(1);	
	}	
    
     *b = abs(a_old + b_old); 
}

int main() {
	    
    int a = 0, b = 0, prev = 0;

    int *pa = &a, *pb = &b;

    unsigned char sign = 0, have_digit = 0;
    
    unsigned char c = fgetc(stdin);


    do		{
			if 	( 
					( c == '-'  )

					&&

					( sign == 0 )
				)
			{
				sign = 1;

				continue;
			}
			
			else if 	( 
						( c == '-'  )

						&&

						( sign == 1 )
					)
			{
					fprintf(stderr,"Error: More than one '-' character in standard input\n");
					exit(1);
			}

			else if 	(
						( c < '0' )

						||

						( c > '9' )	
					)
			{
				fprintf(stderr,"Error: Improperly formatted standard input\n");

				exit(1);
			}
	
			have_digit = 1;

			prev = a;

			a *= 10;
			
			a += (c-'0');

			if ( a < prev )
			{
				fprintf(stderr,"Error: Integer Overflow vulnerability detected while processing variable a\n");

				exit(1);
			}
				
 
		} while	(
				( (c = fgetc(stdin)) != 0xa )
			
				&&

				(
				
					(c == '-')

					||

					(
				
						(c >= '0')

						&&

						(c <= '9')

					)
				
				)

			)
				;


    	if	( c != 0xa || !have_digit )
	{
		fprintf(stderr,"Error: Improperly formatted standard input\n");

		exit(1);
	}

	prev = a;

	if ( sign == 1 )
	{
		a = -a;
	}

	if ( a > prev )
	{
		fprintf(stderr,"Error: Integer Oveflow Vulnerability detected\n");
	
		exit(1);
	}

    have_digit = 0;

    sign = 0;

    prev = 0;
    
    c = fgetc(stdin);
	
    do		{
			if 	( 
					( c == '-'  )

					&&

					( sign == 0 )
				)
			{
				sign = 1;

				continue;
			}
			
			else if 	( 
						( c == '-'  )

						&&

						( sign == 1 )
					)
			{
					fprintf(stderr,"Error: More than one '-' character in standard input\n");
					exit(1);
			}
			
			else if 	(
						( c < '0' )

						||

						( c > '9' )	
					)
			{
				fprintf(stderr,"Error: Improperly formatted standard input\n");

				exit(1);
			}
			
			have_digit = 1;

			prev = b;

			b *= 10;
			
			b += (c-'0');

			if ( b < prev )
			{
				fprintf(stderr,"Error: Integer Overflow vulnerability detected while processing variable b\n");

				exit(1);
			}
				
 
		} while	(
				( (c = fgetc(stdin)) != 0xa )
			
				&&

				(
				
					(c == '-')

					||

					(
				
						(c >= '0')

						&&

						(c <= '9')

					)
				
				)

			)
				;

    	if	( c != 0xa || !have_digit )
	{
		fprintf(stderr,"Error: Improperly formatted standard input\n");

		exit(1);
	}
    
	prev = b;

	if ( sign == 1 )
	{
		b = -b;
	}

	if ( b > prev )
	{
		fprintf(stderr,"Error: Integer Oveflow Vulnerability detected\n");
	
		exit(1);
	}

    update(pa, pb);
    
    printf("%d\n%d", a, b);

    return 0;
}

