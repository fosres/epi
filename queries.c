#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* readline();
char* ltrim(char*);
char* rtrim(char*);
char** split_string(char*);

typedef struct nlist	{

	struct nlist * next;

	int target;

	int freq;
} nlist;

nlist * hashtab[150001];

unsigned long long int fnv(unsigned char *s,unsigned long long int n)
{

	unsigned long long int fnv = 0xcbf29ce484222325;

	unsigned long long int i = 0;

	while ( i < n )
	{ 

		fnv *= 0x100000001b3;

		fnv ^= s[i];

		i++;
	}	

	return fnv % 150001;
}


nlist ** lookup(int query)
{

	nlist * np = (nlist*)calloc(1,sizeof(nlist));

	unsigned char s[5];

	memset(s,0x0,5);

	memcpy(s,&query,4);

	for ( np = hashtab[fnv(s,4)]; np != NULL; np = np->next )
	{
		if ( np->target == query )
		{
			return &np;		
		}
	} 

	return &np;
}

void install(int target)
{
	nlist ** np = lookup(target);

	if ( np == 0 ) { printf("lookup returns NULL!!!\n"); }

	if ( *np == 0 )
	{
		(*np) = (nlist*)calloc(1,sizeof(nlist));
		
		(*np)->target = target;
	
		((*np)->freq)++;		
	}

	else
	{
		((*np)->freq)++;
	}
	
}

int* freqQuery(int queries_rows, int queries_columns, int** queries, int* result_count) 
{

	unsigned i = 0, c = 0;

	while ( i < queries_rows )
	{
		if ( queries[i][0] == 3 )
		{ 
			c++; 
		}

		i++;
	}

	i = 0;

	printf("c: %u\n",c);
	
	*result_count = c;

	int * r = (int*)calloc(c,sizeof(int));

	if ( r == NULL ) { fprintf(stderr,"calloc allocation failed\n"); }

	int * results = r;

	i = 0;

	while ( i < queries_rows )
	{
		if ( queries[i][0] == 1 ) 
		{
			install(queries[i][1]);
		}	

		else if ( queries[i][0] == 2 )
		{

			nlist ** np = lookup(queries[i][1]);

			if ( np == 0 ) { printf("lookup is NULL!!!\n"); }
			
			if ( 

				*np != NULL 

				&&

				( ((*np)->freq) > 0 )
			   )
			{
				((*np)->freq)--;
			}
		}

		else if ( queries[i][0] == 3 )
		{
			unsigned j = 0;
		
			nlist * np = 0;
		
			for ( j = 0; j < 150001; j++ )
			{
				for ( np = hashtab[j] ; np != NULL ; np = np->next )
				{
					if ( np->freq == queries[i][1] )
					{
						*results++ = 1;
					}
				}
			}

		}

		i++;
	}	

	return r;
}


int main()
{
    FILE* fptr = fopen(getenv("OUTPUT_PATH"), "w");

    char* q_endptr;
    char* q_str = ltrim(rtrim(readline()));
    int q = strtol(q_str, &q_endptr, 10);

    if (q_endptr == q_str || *q_endptr != '\0') { exit(EXIT_FAILURE); }

    int** queries = malloc(q * sizeof(int*));

    for (int i = 0; i < q; i++) {
        *(queries + i) = malloc(2 * (sizeof(int)));

        char** queries_item_temp = split_string(rtrim(readline()));

        for (int j = 0; j < 2; j++) {
            char* queries_item_endptr;
            char* queries_item_str = *(queries_item_temp + j);
            int queries_item = strtol(queries_item_str, &queries_item_endptr, 10);

            if (queries_item_endptr == queries_item_str || *queries_item_endptr != '\0') { exit(EXIT_FAILURE); }

            *(*(queries + i) + j) = queries_item;
        }
    }

    int queries_rows = q;
    int queries_columns = 2;

    int ans_count;
    int* ans = freqQuery(queries_rows, queries_columns, queries, &ans_count);

    for (int i = 0; i < ans_count; i++) {
        fprintf(fptr, "%d", *(ans + i));

        if (i != ans_count - 1) {
            fprintf(fptr, "\n");
        }
    }

    fprintf(fptr, "\n");

    fclose(fptr);

    return 0;
}

char* readline() {
    size_t alloc_length = 1024;
    size_t data_length = 0;
    char* data = malloc(alloc_length);

    while (true) {
        char* cursor = data + data_length;
        char* line = fgets(cursor, alloc_length - data_length, stdin);

        if (!line) {
            break;
        }

        data_length += strlen(cursor);

        if (data_length < alloc_length - 1 || data[data_length - 1] == '\n') {
            break;
        }

        alloc_length <<= 1;

        data = realloc(data, alloc_length);

        if (!data) {
            data = '\0';

            break;
        }
    }

    if (data[data_length - 1] == '\n') {
        data[data_length - 1] = '\0';

        data = realloc(data, data_length);

        if (!data) {
            data = '\0';
        }
    } else {
        data = realloc(data, data_length + 1);

        if (!data) {
            data = '\0';
        } else {
            data[data_length] = '\0';
        }
    }

    return data;
}

char* ltrim(char* str) {
    if (!str) {
        return '\0';
    }

    if (!*str) {
        return str;
    }

    while (*str != '\0' && isspace(*str)) {
        str++;
    }

    return str;
}

char* rtrim(char* str) {
    if (!str) {
        return '\0';
    }

    if (!*str) {
        return str;
    }

    char* end = str + strlen(str) - 1;

    while (end >= str && isspace(*end)) {
        end--;
    }

    *(end + 1) = '\0';

    return str;
}

char** split_string(char* str) {
    char** splits = NULL;
    char* token = strtok(str, " ");

    int spaces = 0;

    while (token) {
        splits = realloc(splits, sizeof(char*) * ++spaces);

        if (!splits) {
            return splits;
        }

        splits[spaces - 1] = token;

        token = strtok(NULL, " ");
    }

    return splits;
}

