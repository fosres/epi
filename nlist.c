#include <stdio.h>
#include <stdlib.h>

typedef struct nlist	{
	
	struct nlist * next;

	unsigned int x;

	unsigned int freq;

} nlist;

unsigned fnv_1a(unsigned char * s, unsigned n )
{

	unsigned hash = 2166136261;

	unsigned i = 0;

	while ( i < n )
	{

		hash ^= s[i];

		hash *= 1677719;

		i++;
	}

	return hash;
}

int insert(nlist * table[],unsigned t, int mode)
{

	nlist * x = 0;

	for ( nlist * x = table[fnv_1a(t)] ; ( x->t != 0) && (x->next != 0) && (x->target = t); x = x->next )
		;

	if (
		(x->next == 0)
		
		&&

		(mode == 1)

	)
	{

		x->next = (nlist*)calloc(1,sizeof(nlist));

		x->next->target = t;

		x->next->freq = 1;

		return 1;
	}

	else if
	(
		(x->target == t)

		&&

		(mode == 1)

	)
	{

		x->freq = x->freq + 1;

		return 1;
	}

	else if
	(
		(x->target == t)

		&&

		(mode == 3)
	)
	{
		return 1;
	}

	else if
	(
		(x->target == t)
		
		&&

		(mode == 2)

		&&

		(x->freq > 0)
	)
	{
		x->freq = x->freq - 1;

		return 1;	
	}

	else if
	(
		(x->target == t)
		
		&&

		(mode == 2)

		&&

		(x->freq == 0)
	)
	{
		x->target = 0;

		return 1;	
	}

	else if
	(
		(x->target == 0)

		&&

		(mode == 1)
	)
	{
		x->target = t;

		x->freq = x->freq + 1;

		return 1;
	}

	else if
	(
		( x->target == 0)
		
		&&

		(mode == 2)
	)
	{
		return 0;
	}

	else if
	(
		(x->target == 0)

		&&

		(mode == 3)
	)
	{
		return 0;
	}


}

int * freqQuery(int queries_rows,int queries_columns,int ** queries,int * result_count)
{

	nlist * table[150001];

	unsigned i = 0;

	while ( i < queries_rows )
	{
		if ( queries[i][0] == 3)
		
		i++;
	}

	i = 0;

	*result_count = c;

	int * results = (int*)calloc(c,sizeof(int));

	int * r = result;

	while ( i < queries_rows )
	{
		if (

			(insert(table,queries[i][1],queries[i][0]) == 1)

			&&

			(queries[i][0] == 3)

		)
		{
			*r++ = 1;
		}

		else if ( insert(table,queries[i][1],queries[i][0]) == 0 )
		{
			*r++ = 0;
		}
		
		i++;
	}

	return results;
}
