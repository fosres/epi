#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* readline();
char* ltrim(char*);
char* rtrim(char*);
char** split_string(char*);

// Complete the freqQuery function below.

/*
 * To return the integer array from the function, you should:
 *     - Store the size of the array to be returned in the result_count variable
 *     - Allocate the array statically or dynamically
 *
 * For example,
 * int* return_integer_array_using_static_allocation(int* result_count) {
 *     *result_count = 5;
 *
 *     static int a[5] = {1, 2, 3, 4, 5};
 *
 *     return a;
 * }
 *
 * int* return_integer_array_using_dynamic_allocation(int* result_count) {
 *     *result_count = 5;
 *
 *     int *a = malloc(5 * sizeof(int));
 *
 *     for (int i = 0; i < 5; i++) {
 *         *(a + i) = i + 1;
 *     }
 *
 *     return a;
 * }
 *
 */
typedef struct nlist        {

        unsigned int target;

        unsigned int freq;

    bool neg;

} nlist;

unsigned long long int fnv_1a(unsigned char * s, const unsigned n )
{

        unsigned long long int hash = 0xcbf29ce484222325;

        unsigned long long int i = 0;

        while ( i < n )
        {

                hash ^= s[i];

                hash *= 0x100000001b3;

                i++;
        }

        return hash % n;
}

int insert(nlist * table[],unsigned int freqtable[],unsigned t, int mode)
{
        unsigned char s[5];

        memset(s,0x0,5);

        memcpy(s,&t,4);

    unsigned i = 0, j = 0;

    switch (mode)
    {

        case 1:
        {
            i = fnv_1a(s,4);

            while    (
                    (
         
                        ( i < 100003 )
                    
                        &&

                        ( table[i] != 0 )

                        &&

                        ( (table[i])->target != t )
                        
                    )


                )
            
            {    
                i++;    
            }
            
            if    ( 
                    ( i >= 100003 )

                 )
            {
                return 0;    
            }

            else if    (
                    ( i < 100003 )

                    &&

                    ( table[i] == 0 )

                )

            {
                table[i] = (nlist*)calloc(1,sizeof(nlist));

                (table[i])->target = t;

                (table[i])->freq = 1;

                freqtable[1] = freqtable[1] + 1;

                return 1;
            }

            else if    (
    
                    ( i < 100003 )

                    &&

                    ( table[i] != 0 )
            
                    &&

                    ( (table[i])->target == t )

                )

                {
                    // target about to no longer having that freq

                    freqtable[(table[i])->freq] = freqtable[(table[i])->freq] - 1; 

                    (table[i])->freq = (table[i])->freq + 1;    

                    freqtable[(table[i])->freq] = freqtable[(table[i])->freq] + 1;
                
                    return 1;
                }

                break;        
        }


        case 2:
        {
            i = fnv_1a(s,4);
            
            
            while     ( 
                    ( i < 100003 )
                )
            {
                if     (
                        (i < 100003 )

                        && 
                        
                        (table[i] != 0) 
                    
                        && 
                        
                        ( (table[i])->target == t ) 

                    )
                {
                    break;    
                }
                

                else if ( i < 100003 )
                {
                    i++;
                }
            
            }


    
            if     ( 
                    ( i >= 100003 )
                )
            {
                return 0;    
            }

            else if    ( 
                
                    ( i < 100003 )
                    
                    &&

                    ( (table[i])->target == t )

                )
            {
                

                (freqtable[(table[i])->freq])--;
                
                if     ( 
                        ( (table[i])->freq == 1 )
                    )
                {

                        free(table[i]);

                        table[i] = 0;

                }

                else
                {
                
                    (table[i])->freq = (table[i])->freq - 1;

                    if ( (table[i])->freq != 0 )
                    {
                        (freqtable[(table[i])->freq])++;
                    }
            
                }
        
                return 1;    
            }
            
            break;

        }

        case 3:
        {

            if     ( 
                    ( t <= 100000 ) 

                    &&
                
                    (freqtable[t] != 0 )
                )
            {
                return 1;
            }

            else
            {
                return 0;
            }
        }

    }

    fprintf(stderr,"Error: Incorrect inputs\n");

    return 0;

}

int * freqQuery(int queries_rows,int queries_columns,int ** queries,int * result_count)
{
    nlist * table[100003];

    unsigned int freqtable[100001];

        unsigned i = 0, c = 0;

    memset(table,0x0,100003 * sizeof(nlist*));
    
    memset(freqtable,0x0,100001 * sizeof(unsigned int));
    
    i = 0;

        while ( i < queries_rows )
        {
                if ( queries[i][0] == 3)
        {
            c++;
        }
                
                i++;
        }

        *result_count = c;

        int * results = (int*)calloc(*result_count,sizeof(int));

        int * r = results;

    i = 0;

    unsigned status = 0;

        while ( i < queries_rows )
        {
        status = insert(table,freqtable,queries[i][1],queries[i][0]);
                
        if (

                        (status == 1)

                        &&

                        (queries[i][0] == 3)

                )
                {
                        *r++ = 1;
                }

                else if ( 
            
            (status == 0)

            &&

            (queries[i][0] == 3)
        )
                {
                        *r++ = 0;
                }
                
                i++;
        }

    i = 0;

    while ( i < c )
    {
        printf("%d\n",results[i]);

        i++;
    }

        return results;
}


int main()
{
    FILE* fptr = fopen(getenv("OUTPUT_PATH"), "w");

    char* q_endptr;
    char* q_str = ltrim(rtrim(readline()));
    int q = strtol(q_str, &q_endptr, 10);

    if (q_endptr == q_str || *q_endptr != '\0') { exit(EXIT_FAILURE); }

    int** queries = malloc(q * sizeof(int*));

    for (int i = 0; i < q; i++) {
        *(queries + i) = malloc(2 * (sizeof(int)));

        char** queries_item_temp = split_string(rtrim(readline()));

        for (int j = 0; j < 2; j++) {
            char* queries_item_endptr;
            char* queries_item_str = *(queries_item_temp + j);
            int queries_item = strtol(queries_item_str, &queries_item_endptr, 10);

            if (queries_item_endptr == queries_item_str || *queries_item_endptr != '\0') { exit(EXIT_FAILURE); }

            *(*(queries + i) + j) = queries_item;
        }
    }

    int queries_rows = q;
    int queries_columns = 2;

    int ans_count;
    int* ans = freqQuery(queries_rows, queries_columns, queries, &ans_count);

    for (int i = 0; i < ans_count; i++) {
        fprintf(fptr, "%d", *(ans + i));

        if (i != ans_count - 1) {
            fprintf(fptr, "\n");
        }
    }

    fprintf(fptr, "\n");

    fclose(fptr);

    return 0;
}

char* readline() {
    size_t alloc_length = 1024;
    size_t data_length = 0;
    char* data = malloc(alloc_length);

    while (true) {
        char* cursor = data + data_length;
        char* line = fgets(cursor, alloc_length - data_length, stdin);

        if (!line) {
            break;
        }

        data_length += strlen(cursor);

        if (data_length < alloc_length - 1 || data[data_length - 1] == '\n') {
            break;
        }

        alloc_length <<= 1;

        data = realloc(data, alloc_length);

        if (!data) {
            data = '\0';

            break;
        }
    }

    if (data[data_length - 1] == '\n') {
        data[data_length - 1] = '\0';

        data = realloc(data, data_length);

        if (!data) {
            data = '\0';
        }
    } else {
        data = realloc(data, data_length + 1);

        if (!data) {
            data = '\0';
        } else {
            data[data_length] = '\0';
        }
    }

    return data;
}

char* ltrim(char* str) {
    if (!str) {
        return '\0';
    }

    if (!*str) {
        return str;
    }

    while (*str != '\0' && isspace(*str)) {
        str++;
    }

    return str;
}

char* rtrim(char* str) {
    if (!str) {
        return '\0';
    }

    if (!*str) {
        return str;
    }

    char* end = str + strlen(str) - 1;

    while (end >= str && isspace(*end)) {
        end--;
    }

    *(end + 1) = '\0';

    return str;
}

char** split_string(char* str) {
    char** splits = NULL;
    char* token = strtok(str, " ");

    int spaces = 0;

    while (token) {
        splits = realloc(splits, sizeof(char*) * ++spaces);

        if (!splits) {
            return splits;
        }

        splits[spaces - 1] = token;

        token = strtok(NULL, " ");
    }

    return splits;
}

