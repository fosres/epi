#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* readline();
char* ltrim(char*);
char* rtrim(char*);
char** split_string(char*);

typedef unsigned long long int uint64;

typedef struct nlist
{
	struct nlist * l;
		
	uint64 freq;

	long x;

} nlist;

uint64 fnv_1a(unsigned char *s,uint64 n)
{
	uint64 hash = 0xcbf29ce484222325;

	while ( *s != 0 )
	{
		hash ^= *s;
		
		hash *= 0x100000001b3;		
		
		s++;
	}

	return hash % n;

}

uint64 fact(uint64 n, uint64 b)
{
	if ( n == 1 || n == 0 )
	{
		return 1;
	}

	if ( b > n )
	{
		return 0;
	}

	uint64 prod = 1, i = b;

	while ( i <= n )
	{
		prod *= i;

		i++;
	}

	return prod;
}

uint64 ncr(uint64 n, uint64 r)
{
	if ( n < r )
	{
		return 0;
	}
	
	uint64 b = n - r + 1;

	return ( fact(n,b)/( fact(r,1) ) );
}

// Complete the countTriplets function below.
long countTriplets(int arr_count, long* arr, long r) {

	nlist * hashtable[100003];

	memset(hashtable,0x0,100003*sizeof(nlist*));

	unsigned freqtable[100001];

	memset(freqtable,0x0,100001*sizeof(unsigned));

	unsigned i = 0, j = arr_count - 1, m = 0, n = 0;

	nlist ** k = (nlist**)calloc(1,sizeof(nlist*));

	unsigned table[32];

	memset(table,0x0,32*sizeof(unsigned));

	if ( r != 1 )
	{
		while ( i < j )
		{
			if 	( 
					(arr[i] % r == 0 )
				
				)
	
			{

				printf("%u\n",(unsigned)(log(arr[i])/(double)log(r)));

				table[(unsigned)(log(arr[i])/(double)log(r))]++;
			
			}

			else if	(
					(arr[i] == 1)
				)
			{
				printf("%u\n",(unsigned)(log(arr[i])/(double)log(r)));

				table[0]++;

			}
		
			if 	( 
					(arr[j] % r == 0 )
				)
			{
				printf("%u\n",(unsigned)(log(arr[j])/(double)log(r)));

				table[(unsigned)(log(arr[j])/log(r))]++;
			}

			else if	(
					(arr[j] == 1)
				)
			{
				printf("%u\n",(unsigned)(log(arr[j])/(double)log(r)));

				table[0]++;

			}
	
			i++; j--;
		}

		if ( i == j )
		{
			if 	( 
					(arr[i] % r == 0 )
				)
			{
				table[(unsigned)(log(arr[i])/log(r))]++;
			}

			else if	(
					(arr[i] == 1)
				)
			{
				table[0]++;
			}
		}

		i = 0;

		while ( i < 32 )
		{

			printf("%ld ",table[i]);

			i++;
		}

		printf("\n");

		i = 0; j = i + 1; m = j + 1;

		long c = 0;

		while 	(
				(i < 32)

				&&
				
				(j < 32)
				
				&&

				(m < 32)
			)
		{
			c += (table[i]*table[j]*table[m]);
			
			i++; j++; m++;

		}

		printf("%ld\n",c);

		return c;			
	}

	i = 0; j = arr_count - 1; m = 0; n = 0;
	
	for ( ; i <= j; i++,j-- )
	{
		m = fnv_1a(&arr[i],100003);

		printf("Got m\n %ld\n",m);

		n = fnv_1a(&arr[j],100003);
		
		printf("Got n\n %ld\n",n);

		for 	( 	
				k = &hashtable[m]
			
				;

				( (*k) != 0) && ( ( (*k)->x ) != arr[i])
				
				;

				k = &( (*k)->l )
			)
				;

		
		if	( (*k) == 0 )
		{
			(*k) = (nlist*)calloc(1,sizeof(nlist));
			
			(*k)->freq = 1;

			freqtable[1]++;

			(*k)->x = arr[i];
		}	

		else
		{
			freqtable[(*k)->freq]--;

			( (*k)->freq )++;
			
			freqtable[(*k)->freq]++;
		}
	
		if ( i == j )
		{
			continue;
		}
	
		for 	( 	
				k = &hashtable[n]
			
				;

				( (*k) != 0) && ( ( (*k)->x ) != arr[j])
				
				;

				k = &( (*k)->l )
			)
			;

		
		if	( (*k) == 0 )
		{
			(*k) = (nlist*)calloc(1,sizeof(nlist));
			
			(*k)->freq = 1;

			freqtable[1]++;

			(*k)->x = arr[j];
		}	

		else
		{
			freqtable[(*k)->freq]--;

			( (*k)->freq )++;
			
			freqtable[(*k)->freq]++;
		}

		
	}

	long c = 0;

	printf("Made up to this point\n");

	// ncr(i,3) becomes indescribably large when at the edge of the freqtable XD

	for ( i = 3, j = 100000; i < j; i++, j-- )
	{
		c += ( freqtable[i] * ncr(i,3) );
		
		c += ( freqtable[j] * ncr(j,3) );
	}	

	if ( i == j )
	{
		c += ( freqtable[i] * ncr(i,3) );
	}

	printf("%ld\n",c);

	return c;
}

int main()
{
    FILE* fptr = fopen(getenv("OUTPUT_PATH"), "w");

    char** nr = split_string(rtrim(readline()));

    char* n_endptr;
    char* n_str = nr[0];
    int n = strtol(n_str, &n_endptr, 10);

    if (n_endptr == n_str || *n_endptr != '\0') { exit(EXIT_FAILURE); }

    char* r_endptr;
    char* r_str = nr[1];
    long r = strtol(r_str, &r_endptr, 10);

    if (r_endptr == r_str || *r_endptr != '\0') { exit(EXIT_FAILURE); }

    char** arr_temp = split_string(rtrim(readline()));

    long* arr = malloc(n * sizeof(long));

    for (int i = 0; i < n; i++) {
        char* arr_item_endptr;
        char* arr_item_str = *(arr_temp + i);
        long arr_item = strtol(arr_item_str, &arr_item_endptr, 10);

        if (arr_item_endptr == arr_item_str || *arr_item_endptr != '\0') { exit(EXIT_FAILURE); }

        *(arr + i) = arr_item;
    }

    int arr_count = n;

    long ans = countTriplets(arr_count, arr, r);

    fprintf(fptr, "%ld\n", ans);

    fclose(fptr);

    return 0;
}

char* readline() {
    size_t alloc_length = 1024;
    size_t data_length = 0;
    char* data = malloc(alloc_length);

    while (true) {
        char* cursor = data + data_length;
        char* line = fgets(cursor, alloc_length - data_length, stdin);

        if (!line) {
            break;
        }

        data_length += strlen(cursor);

        if (data_length < alloc_length - 1 || data[data_length - 1] == '\n') {
            break;
        }

        alloc_length <<= 1;

        data = realloc(data, alloc_length);

        if (!data) {
            data = '\0';

            break;
        }
    }

    if (data[data_length - 1] == '\n') {
        data[data_length - 1] = '\0';

        data = realloc(data, data_length);

        if (!data) {
            data = '\0';
        }
    } else {
        data = realloc(data, data_length + 1);

        if (!data) {
            data = '\0';
        } else {
            data[data_length] = '\0';
        }
    }

    return data;
}

char* ltrim(char* str) {
    if (!str) {
        return '\0';
    }

    if (!*str) {
        return str;
    }

    while (*str != '\0' && isspace(*str)) {
        str++;
    }

    return str;
}

char* rtrim(char* str) {
    if (!str) {
        return '\0';
    }

    if (!*str) {
        return str;
    }

    char* end = str + strlen(str) - 1;

    while (end >= str && isspace(*end)) {
        end--;
    }

    *(end + 1) = '\0';

    return str;
}

char** split_string(char* str) {
    char** splits = NULL;
    char* token = strtok(str, " ");

    int spaces = 0;

    while (token) {
        splits = realloc(splits, sizeof(char*) * ++spaces);

        if (!splits) {
            return splits;
        }

        splits[spaces - 1] = token;

        token = strtok(NULL, " ");
    }

    return splits;
}

